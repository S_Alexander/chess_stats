import argparse
import requests
import json
import time
import datetime

def make_request(url):
    while True:
        time.sleep(1)
        response = requests.get(url)
        if (response.status_code == 200): # ok
            return json.loads(response.content)
        elif (response.status_code == 429): # too many requests
            print("too many")
            time.sleep(60)
        else:
            print("Bad response: {0}".format(response.status_code))
            exit()

def url_from_id(id):
    url = "https://lichess.org/api/game/{0}".format(id)
    flags = "?with_movetimes=1&with_analysis=1&with_moves=1"
    return url + flags

def url_vs(pl1, pl2):
    return "https://lichess.org/api/games/vs/{0}/{1}".format(pl1, pl2)
            
def check(game, start, finish):
    if (game['clock']['initial'] != 5400):
        return False
    if (game['clock']['increment'] != 30):
        return False
    if (game['createdAt'] < start):
        return False
    if (game['createdAt'] > finish):
        return False
    return True
            
def clean(data, start, finish):
    if data['nbResults'] > 9:
        print("nbResults > 9")
        exit()
    games = []
    for g in data['currentPageResults']:
        if check(g, start, finish):
            games.append(g)
    if len(games) > 1:
        print("games > 1")
        exit()
    if len(games) == 1:
        return games[0]
    else:
        return None
            
def fetch_game(pl1, pl2, start, finish):
    data = make_request(url_vs(pl1, pl2))
    game = clean(data, start, finish)
    return game
            
def read_file(file):
    pairs = set()
    f = open(file, "r")
    for line in f:
        splits = line.strip('\n').split('\t')
        pl1 = splits[0].lower()
        pl2 = splits[6].lower()
        pair = frozenset([pl1, pl2])
        pairs.add(pair)
    return pairs
            
exclude = {'free spot'}
            
def process_pairs(file, start, finish):
    pairs = read_file(file)
    games = []
    for pair in pairs:
        if (pair & exclude) == set():
            pl1, pl2 = pair
            game = fetch_game(pl1, pl2, start, finish)
            if game is not None:
                games.append(game)
    return games
            
def valid_date(s):
    try:
        dt = datetime.datetime.strptime(s, '%Y-%m-%d')
        dt = dt.replace(tzinfo=datetime.timezone.utc)
        return dt
    except ValueError:
        msg = "Not a valid date: '{0}'.".format(s)
        raise argparse.ArgumentTypeError(msg)
            
def main():
    parser = argparse.ArgumentParser(description="Python script for collecting "
                                                 "chess stats")
    parser.add_argument('-ids', nargs='+', help="fetch games by ids")
    parser.add_argument('-pairs', help="file with pairings")
    parser.add_argument('-start', type=valid_date,
                        help="starting date (including)")
    parser.add_argument('-finish', type=valid_date,
                        help="finishing data (including)")
    args = parser.parse_args()
    start = 0
    if args.start is not None:
        start = int(args.start.timestamp())
        start *= 1000
    finish = float('inf')
    if args.finish is not None:
        finish = int(args.finish.timestamp() + 24 * 60 * 60 - 1)
        finish *= 1000
    games = []
    if args.pairs is not None:
        games = process_pairs(args.pairs, start, finish)
    if args.ids is not None:
        for id in args.ids:
            game = make_request(url_from_id(id))
            games.append(game)
    for g in games:
        print("{0} vs {1}".format(g['players']['white']['userId'], g['players']['black']['userId']))
        
        
            
if __name__ == "__main__":
    main()